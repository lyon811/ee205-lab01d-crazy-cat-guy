###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d- Crazy Cat Guy - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.1 - Initial version
###
### Build a Crazy Cat guy C program
###
### @author  Lyon <lyonws@hawaii.edu>
### @date     01_13_2021
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = crazyCatGuy

all: $(TARGET)

crazyCatGuy: crazyCatGuy.c
	$(CC) $(CFLAGS) -o $(TARGET) crazyCatGuy.c

clean:
	rm -f $(TARGET) *.o
	










